FROM ubuntu:18.04

# install dependencies
RUN apt-get update
ENV DEBIAN_FRONTEND="noninteractive" TZ="Asia/Kolkata"
RUN apt-get install -y libopencv-dev liblinear-dev libsvm-dev libtiff5-dev

COPY include/ /usr/include
COPY lib/ /usr/lib
RUN cd /usr/lib && ln -s libprotobuf.so.20 libprotobuf.so && ln -s libtensorflow_cc.so.2 libtensorflow_cc.so
